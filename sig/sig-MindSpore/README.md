# MindSpore SIG

-   为了MindSpore后续在openEuler上能稳定运行，通过建立MindSpore项目SIG，将MindSpore依赖的开源软件依托在src-openEuler进行维护.
-   扩展openEuler社区的AI训练框架生态 
-   构建MindSpore依赖的开源软件在openEuler维护能力
-   提供AI训练框架软件开源承载平台



# 组织会议

- 公开的会议时间：NA
- 邮件讨论为主，涉及决策的内容，TC会议申报议题



# 成员

### Maintainer列表

- 欧功畅[@ouwenchang](https://gitee.com/ouwenchang)，*ougongchang@huawei.com*
- 金小贤[@kingxian](https://gitee.com/kingxian)
- 郭琦[@guoqi1024](https://gitee.com/guoqi1024)
- 鲍翀[@Baochong](https://gitee.com/Baochong)
- 程现斌[@chengxb7532](https://gitee.com/chengxb7532)
- 朱乃盘[@Zhunaipan](https://gitee.com/Zhunaipan)



### Committer列表

- 欧功畅[@ouwenchang](https://gitee.com/ouwenchang)
- 周培晨[@zpac](https://gitee.com/zpac)
- 周姗[@zhoushan33](https://gitee.com/zhoushan33)
- 金小贤[@kingxian](https://gitee.com/kingxian)
- 郭琦[@guoqi1024](https://gitee.com/guoqi1024)
- 鲍翀[@Baochong](https://gitee.com/Baochong)
- 程现斌[@chengxb7532](https://gitee.com/chengxb7532)
- 朱乃盘[@Zhunaipan](https://gitee.com/Zhunaipan)



# 联系方式

- [邮件列表](dev@openeuler.org)
- [IRC公开会议]()
- 视频会议





# 项目清单

*<可选，如果在申请SIG的时候，就有新项目，请完善此处内容。项目名称和申请表格一致，repository地址和repository.yaml内的申请地址一致>*

项目名称：

repository地址：

- https://gitee.com/src-openeuler/okhttp